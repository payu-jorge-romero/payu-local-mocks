const path = require('path');
const fs = require("fs"); 

/**
 * Add the mock routes to server app
 * @param {ExpressServerApp} server 
 * @param {String} contextPath the main context path for server app
 */
var addMockRoutes = function (server, contextPath, hostApi) {
    // Add custom routes before JSON Server router
    server.get('/', (req, res) => {
        res.jsonp({"run":"ok"})
    });

    // Add custom routes before JSON Server router
    server.get(contextPath + '/echo', (req, res) => {
        res.jsonp(req.baseUrl)
    });

    // mock case1 auth login end-point
    server.get(contextPath + '/maf.pagosonline.net/ws/rs/iin/:bin', (req, res) => {
        console.log('maf.pagosonline.net/ws/rs/iin (bin): ', req.params.bin);
        var mockJsonPath = path.join(__dirname, './mock_json_data/bin_response_'+req.params.bin+'.json')
        var binResponse = null;
        if (fs.existsSync(mockJsonPath)) {
            // Do something
            binResponse = require(mockJsonPath);
        }else {
            // default response
            binResponse = require(path.join(__dirname, './mock_json_data/bin_default_response.json'));
        }
        binResponse.findIINMafResponse.iin = req.params.bin
        console.log("binResponse: ", binResponse);
        res.jsonp(binResponse);
    });


    var frautVauldContex = contextPath + '/fraut-vauld';
    // mock case1 auth login end-point
    server.post(frautVauldContex + '/v1/auth/approle/login', (req, res) => {
        console.log(frautVauldContex + '/v1/auth/approle/login', req.body);
        var loginResponse = require(path.join(__dirname, './mock_json_data/fraut_vauld_approle_login_reponse.json'));
        console.log("fraut_vauld_approle_login_reponse: ", loginResponse);
        res.jsonp(loginResponse);
    });

    loggerMainRoutesFromServerApp(contextPath, server, hostApi);
};

/**
 * Log from server app for main added routes
 * @param {ExpressServerApp} server 
 */
var loggerMainRoutesFromServerApp = function (contextPath, server, hostApi) {
    server._router.stack.forEach(function (entry) {
        if (entry.route) {
            console.log('-----> add path: ', hostApi + entry.route.path);
            console.log('methods :', entry.route.methods);
        }
    });
};

exports.addMockRoutes = addMockRoutes;
