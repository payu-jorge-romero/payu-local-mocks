// server.js
const jsonServer = require('json-server');
const server = jsonServer.create();
const cors = require('cors');
const middlewares = jsonServer.defaults();
// db local json data
const path = require('path');
const router = jsonServer.router(path.join(__dirname, '/data/db.json'));
const contextPath = '/payu-local-mocks'
const serverPort = 3009;
const serverHost = 'http://localhost';

console.log("-------------> payu-local-mocks <--------------")

// add cors config to server
server.use(cors());

// add middlewares config
server.use(middlewares);

// To handle POST, PUT and PATCH you need to use a body-parser
// You can use the one used by JSON Server
server.use(jsonServer.bodyParser);

// add to default requests POST the parameter createdAt with the current date
server.use((req, res, next) => {
    if (req.method === 'POST') {
        req.body.createdAt = Date.now()
    }
    // Continue to JSON Server router
    next()
});

var hostApi = serverHost + ':' + serverPort;

const mockRoutes = require('./mock_routes.js');
mockRoutes.addMockRoutes(server, contextPath, hostApi);

// Use default router, add initial context path mock api and json router data db
server.use(contextPath, router);
server.listen(serverPort, () => {
    console.log('JSON-Server is running: ' + hostApi)
});
