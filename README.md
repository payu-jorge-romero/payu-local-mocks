# payu-local-mocks

### Bines

### VISA CREDIT CARD PE
`BIN`: 403530
`CREDIT CARD EXAMPLE`: 4035300539804083

### VISA DEBIT CARD PE
`BIN`: 452109
`CREDIT CARD EXAMPLE`: 4521090339767454

### MASTERCARD CREDIT CARD PE
`BIN`: 532755
`CREDIT CARD EXAMPLE`: 5327550787467546

### MASTERCARD DEBIT CARD PE
`BIN`: 524837
`CREDIT CARD EXAMPLE`: 5248372908849619

### DINERS DEBIT CARD PE
`BIN`: 368456
`CREDIT CARD EXAMPLE`: 36845690574419

### AMEX DEBIT CARD PE
`BIN`: 371642
`CREDIT CARD EXAMPLE`: 371642190784801

### Running json-server

This example shows, how to run a local json-server for development purposes.

Edit `db.json` to alter the data, received from the endpoint.

This example does not use a global installation of json-server.

Find more details on the original repository of [json-server][json-server]

#### Usage

Clone the repository and start your own project.
Install the latest [Node.js](https://nodejs.org/en/), install dependencies and start the server.

```bash
$ npm i      # install deps
$ npm start  # start the server, using script
```

#### Use in JavaScript

You might use [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) to send a request to the endpoint.

```js
var request = new Request('http://localhost:6666/posts/', {
  headers: new Headers({
    'Content-Type': 'application/json'
  })
});

fetch(request)
  .then(function(res) {
    return res.json();
  })
  .then(function(data) {
    // Response data
  })
  .catch(function(err) {
    // Error
  });
```

[json-server]: <https://github.com/typicode/json-server>

## references, additional data:

- http://json2ts.com/ (generates typescript interfaces from json config)
- https://github.com/typicode/json-server


Ideal http codes and verbs responses:
- https://solidgeargroup.com/en/best-practices-rest-api/
